﻿using System;
using Cysharp.Threading.Tasks;
using Runtime.Creatures;
using Runtime.Effects;
using Runtime.HUD;
using Runtime.Player;
using Runtime.Projectiles;
using Runtime.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace Runtime
{
    public enum TimeMode
    {
        Realtime,
        TurnBased
    }
    
    public class World : MonoBehaviourSingleton<World>
    {
        public ViewportClickHandler viewportClickHandler;
        public DifficultySettings difficultySettings;
        public HudController hudController;
        public EffectsManager effectsManager;
        public ProjectilesManager projectilesManager;
        public PlayerController playerController;
        public CreatureManager CreatureManager;
        
        public TimeMode TimeMode { private set; get; }
        public readonly GameTime GameTime = new GameTime();
        
        public readonly UnityEvent<TimeMode> OnTimeModeChanged = new UnityEvent<TimeMode>();
        public readonly UnityEvent OnCreatureTurn = new UnityEvent();
        public readonly UnityEvent OnPlayerTurn = new UnityEvent();

        private const float TargetHeight = 540.0f;

        private void Start()
        {
            SetupRenderScale();
            
            GameTime.Time = DateTime.Parse("08/18/1776 07:22:16");
            
            playerController.Initialize();
            
            CreatureManager = new CreatureManager();
            
            SetTimeMode(TimeMode.Realtime);
        }

        private void SetupRenderScale()
        {
            var rpAsset = UnityEngine.Rendering.GraphicsSettings.renderPipelineAsset;
            var urpAsset = (UnityEngine.Rendering.Universal.UniversalRenderPipelineAsset) rpAsset;
 
            urpAsset.renderScale = Mathf.Min (TargetHeight / Screen.height, 1.0f);
            Debug.Log("Render Scale is: " + urpAsset.renderScale);
        }

        private async UniTaskVoid RealtimeRoutine()
        {
            CreatureManager.SetTurnBasedBlock(false);

            while (true)
            {
                if (TimeMode != TimeMode.Realtime)
                {
                    break;
                }
                
                await UniTask.Delay(TimeSpan.FromSeconds(1f));
                
                GameTime.Advance1Second();
            }
        }
        
        private async UniTaskVoid TurnBasedRoutine()
        {
            var counter = 0f;
            var penalty = difficultySettings.turnBasedPenalty;
            while (true)
            {
                if (TimeMode != TimeMode.TurnBased)
                {
                    break;
                }
                
                OnCreatureTurn.Invoke();
                CreatureManager.UpdateRealtime();
                
                if (penalty > 0f)
                {
                    penalty -= Time.deltaTime;
                }
                else
                {
                    if (playerController.UpdateCurrentCharacter())
                    {
                        OnPlayerTurn.Invoke();
                        CreatureManager.SetTurnBasedBlock(true);
                        await playerController.UpdatePlayerTurnBased();
                        CreatureManager.SetTurnBasedBlock(false);
                    }
                }
                
                counter += Time.deltaTime;
                if (counter > 1f)
                {
                    counter = 1 - counter;
                    GameTime.Advance1Second();
                }
                
                await UniTask.Yield();
            }
        }

        public void SetTimeMode(TimeMode timeMode)
        {
            TimeMode = timeMode;
            switch (timeMode)
            {
                case TimeMode.Realtime:
                    RealtimeRoutine().Forget();
                    break;
                case TimeMode.TurnBased:
                    TurnBasedRoutine().Forget();
                    break;
            }
            OnTimeModeChanged.Invoke(timeMode);
        }

        private void Update()
        {
            playerController.UpdatePlayerGeneral();
            
            if (TimeMode == TimeMode.Realtime)
            {
                playerController.UpdatePlayerRealtime();
                CreatureManager.UpdateRealtime();
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (TimeMode == TimeMode.Realtime)
                {
                    SetTimeMode(TimeMode.TurnBased);
                }
                else
                {
                    SetTimeMode(TimeMode.Realtime);
                }
            }
            
            CreatureManager.RenderAll();
        }
    }
}
