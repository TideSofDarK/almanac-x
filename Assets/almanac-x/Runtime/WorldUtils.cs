﻿using UnityEngine;

namespace Runtime
{
    public class WorldUtils
    {
        public static LayerMask CreatureMask = LayerMask.GetMask("Creatures");
        public static LayerMask ClickableMask = LayerMask.GetMask("Creatures", "Ignore Raycast");
        
        public static int RagdollLayer = LayerMask.NameToLayer("Ragdoll");
    }
}