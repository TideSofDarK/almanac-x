﻿using UnityEngine;

namespace Runtime
{
    [CreateAssetMenu(fileName = "DifficultySettings", menuName = "almanac/Difficulty Settings", order = 0)]
    public class DifficultySettings : ScriptableObject
    {
        public float turnBasedPenalty = 1f;
    }
}