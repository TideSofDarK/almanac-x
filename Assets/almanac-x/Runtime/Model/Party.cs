﻿using System;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Runtime.Model
{
    [Serializable]
    public class Party
    {
        public static readonly int MaxCharacters = 4;
        
        public readonly PlayerCharacter[] Characters;

        [NonSerialized] public readonly UnityEvent OnPartyDamageTaken = new UnityEvent();

        [NonSerialized] private bool _partyWasChangedThisFrame;

        public Party()
        {
            Characters = new PlayerCharacter[4];

            for (int i = 0; i < MaxCharacters; i++)
            {
                Characters[i] = new PlayerCharacter(100);
                
                Characters[i].OnDamageTaken.AddListener(() => _partyWasChangedThisFrame = true);
            }
        }

        public PlayerCharacter GetDamageTarget()
        {
            return Characters[Random.Range(0, MaxCharacters)];
        }

        public void Update()
        {
            if (_partyWasChangedThisFrame)
            {
                OnPartyDamageTaken.Invoke();
                _partyWasChangedThisFrame = false;
            }
        }
    }
}