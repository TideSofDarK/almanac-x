﻿namespace Runtime.Model
{
    public abstract class CreatureBase
    {
        public int Health { private set; get; }
        public int MaxHealth { private set; get; }
        
        protected CreatureBase(int maxHealth)
        {
            MaxHealth = maxHealth;
            Health = MaxHealth;
        }
        
        public virtual void TakeDamage(int damage)
        {
            Health -= damage;
        }

        public virtual bool IsDead()
        {
            return Health <= 0;
        }
    }
}