﻿using System;

namespace Runtime.Model
{
    [Serializable]
    public class Weapon
    {
        public const float MeleeRange = 2.75f;
        
        public WeaponClass weaponClass;
        public Dice attackDice;

        public WeaponType WeaponType
        {
            get
            {
                switch (weaponClass)
                {
                    case WeaponClass.Crossbow:
                        return WeaponType.Ranged;
                    case WeaponClass.Sword:
                    default:
                        return WeaponType.Melee;
                }
            }
        }
    }
}