﻿namespace Runtime.Model
{
    public class Monster : CreatureBase
    {
        public readonly Dice AttackDice;

        public Monster(int maxHealth, Dice attackDice) : base(maxHealth)
        {
            AttackDice = attackDice;
        }
    }
}