﻿using System;
using Random = UnityEngine.Random;

namespace Runtime.Model
{
    [Serializable]
    public struct Dice
    {
        public int numberOfDice;
        public int diceValue;
        public int modifier;

        public Dice(int numberOfDice, int diceValue, int modifier)
        {
            this.numberOfDice = numberOfDice;
            this.diceValue = diceValue;
            this.modifier = modifier;
        }
        
        // @TODO: readonly C# 8.0 feature
        public int Roll()
        {
            var result = 0;
            for (var i = 0; i < numberOfDice; i++)
            {
                result += Random.Range(1, diceValue + 1);
            }
            return result + modifier;
        }

        public override string ToString()
        {
            return $"{numberOfDice}d{diceValue}+{modifier}";
        }
    }
}