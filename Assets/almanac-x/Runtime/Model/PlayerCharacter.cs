﻿using System;
using UnityEngine.Events;

namespace Runtime.Model
{
    [Serializable]
    public class PlayerCharacter : CreatureBase
    {
        public int strength = 8;
        public int constitution = 8;
        public int agility = 8;
        public int accuracy = 8;
        public int intellect = 8;
        public int wisdom = 8;
        public int speed = 8;
        public int luck = 8;
        public int charisma = 8;
        public int faith = 8;
        
        public Weapon weapon;
        
        public static readonly Dice UnarmedAttackDice = new Dice(1, 4, 0);
        
        [NonSerialized] public readonly UnityEvent OnDamageTaken = new UnityEvent();

        [NonSerialized] public DateTime OnCooldownUntil;
        [NonSerialized] public float CooldownDuration;

        public PlayerCharacter(int maxHealth) : base(maxHealth)
        {
        }

        public Dice GetAttackDice()
        {
            return weapon?.attackDice ?? UnarmedAttackDice;
        }

        public override bool IsDead()
        {
            return Health <= -MaxHealth;
        }

        public override void TakeDamage(int damage)
        {
            base.TakeDamage(damage);
            OnDamageTaken.Invoke();
        }
    }
}