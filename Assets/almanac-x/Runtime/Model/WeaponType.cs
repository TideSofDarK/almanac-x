﻿namespace Runtime.Model
{
    public enum WeaponType
    {
        Ranged,
        Melee
    }
}