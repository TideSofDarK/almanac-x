﻿using Cysharp.Threading.Tasks;
using Runtime.Creatures.AI;
using Runtime.Model;
using Runtime.Player.Actions;
using Runtime.Weapons;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Runtime.Player
{
    public class PlayerController : MonoBehaviour, IAiTarget
    {
        private const int NoActiveCharacter = -1;

        public new Camera camera; 
        public CharacterController characterController;

        public Party party;
        
        private PlayerMovement _movement;
        private PlayerActionHandler _actionHandler;

        public GameObject currentTarget;
        public float currentDistanceToTarget;

        public int currentCharacterIndex;
        public readonly UnityEvent<int, WeaponDefinition> OnWeaponEquipped = new UnityEvent<int, WeaponDefinition>();
        public readonly UnityEvent<int> OnCurrentCharacterChanged = new UnityEvent<int>();
        public readonly UnityEvent<int> OnCharacterSetOnCooldown = new UnityEvent<int>();
        public PlayerCharacter CurrentCharacter => party.Characters[currentCharacterIndex];

        public WeaponDefinition TestSword, TestCrossbow;
        
        public void Initialize()
        {
            party = new Party();

            _movement = new PlayerMovement(characterController);
            _actionHandler = new PlayerActionHandler(this);
        }

        private void Start()
        {
            EquipTestWeapons();
        }

        public void EquipTestWeapons()
        {
            for (var i = 0; i < 4; i++)
            {
                EquipWeapon(i, Random.Range(0,2) == 0 ? TestSword : TestCrossbow);
            }
        }

        private void UpdatePointerTarget()
        {
            var ray = World.Get().viewportClickHandler.GetViewportRay();
            Physics.Raycast(ray, out var hitInfo, 100f, WorldUtils.ClickableMask);
            if (hitInfo.transform)
            {
                World.Get().hudController.pointerCaption.text = hitInfo.transform.name;
                currentTarget = hitInfo.transform.gameObject;
                currentDistanceToTarget = hitInfo.distance;
            }
            else
            {
                World.Get().hudController.pointerCaption.text = "Nothing";
                currentTarget = null;
            }
        }

        public void EquipWeapon(int characterIndex, WeaponDefinition weaponDefinition)
        {
            party.Characters[characterIndex].weapon = weaponDefinition.model;
            OnWeaponEquipped.Invoke(characterIndex, weaponDefinition);
        }

        public async UniTask UpdatePlayerTurnBased()
        {
            UpdateCurrentCharacter();

            while (true)
            {
                if (!await UpdatePlayerIfReady())
                {
                    break;
                }

                await UniTask.Yield();
            }
        }

        public void UpdatePlayerRealtime()
        {
            UpdateCurrentCharacter();
            
            _movement.UpdateMovement();
            
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                NextCharacter();
            }
            
            UpdatePlayerIfReady();
        }

        private async UniTask<bool> UpdatePlayerIfReady()
        {
            if (currentCharacterIndex == NoActiveCharacter)
            {
                return false;
            }

            if (!IsCharacterReady(currentCharacterIndex)) return true;
            
            var action = _actionHandler.Get(CurrentCharacter);
            if (action == null) return true;
            
            SetCurrentCharacterOnCooldown(5f);
            NextCharacter();
            
            await action.Perform();

            return true;
        }

        public bool UpdateCurrentCharacter()
        {
            if (currentCharacterIndex != NoActiveCharacter && IsCharacterReady(currentCharacterIndex)) return true;
            
            for (var i = 0; i < 4; i++)
            {
                if (!IsCharacterReady(i)) continue;
                    
                SetCurrentCharacter(i);
                return true;
            }

            return false;
        }

        public void UpdatePlayerGeneral()
        {
            _movement.UpdateLook();
            _movement.ApplyMovementAndGravity();

            UpdatePointerTarget();

            party.Update();
        }

        private void SetCurrentCharacterOnCooldown(float duration)
        {
            CurrentCharacter.CooldownDuration = duration;
            CurrentCharacter.OnCooldownUntil = World.Get().GameTime.Time.AddSeconds(duration);
            OnCharacterSetOnCooldown.Invoke(currentCharacterIndex);
        }

        private bool IsCharacterReady(int characterIndex)
        {
            return !party.Characters[characterIndex].IsDead() && World.Get().GameTime.Time >= party.Characters[characterIndex].OnCooldownUntil;
        }

        private bool NextCharacter()
        {
            var lastCharacterIndex = currentCharacterIndex;
            while (true)
            {
                lastCharacterIndex = (int) Mathf.Repeat(lastCharacterIndex + 1, 4);
                if (lastCharacterIndex == currentCharacterIndex)
                {
                    SetCurrentCharacter(NoActiveCharacter);
                    return false;
                }
                if (IsCharacterReady(lastCharacterIndex))
                {
                    SetCurrentCharacter(lastCharacterIndex);
                    return true;
                }
            }
        }

        private void SetCurrentCharacter(int characterIndex)
        {
            currentCharacterIndex = characterIndex;
            OnCurrentCharacterChanged.Invoke(characterIndex);
        }

        public Vector3 GetAiTargetPosition()
        {
            return transform.position;
        }

        public CreatureBase GetAttackTarget()
        {
            return party.GetDamageTarget();
        }
    }
}