﻿using Cysharp.Threading.Tasks;

namespace Runtime.Player.Actions
{
    public class PlayerRangedAttackAction : PlayerAttackAction
    {
        public override async UniTask Perform()
        {
            var start = World.Get().playerController.transform.position;
            var target = await World.Get().projectilesManager.SpawnArrow(start, TargetCreature.GetMiddlePoint() - start);
            if (target)
            {
                target.TakeDamage(PlayerCharacter.GetAttackDice().Roll());
            }
        }
    }
}