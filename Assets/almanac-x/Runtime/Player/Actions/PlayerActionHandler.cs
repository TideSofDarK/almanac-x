﻿using Runtime.Creatures;
using Runtime.Model;
using UnityEngine;

namespace Runtime.Player.Actions
{
    public class PlayerActionHandler
    {
        private readonly PlayerController _playerController;

        public PlayerActionHandler(PlayerController playerController)
        {
            _playerController = playerController;
        }

        public PlayerAction Get(PlayerCharacter playerCharacter)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (_playerController.currentTarget &&
                    _playerController.currentTarget.GetComponent<CreatureController>() is var creatureController)
                {
                    return CreateAttackAction(playerCharacter, creatureController);
                }
            }

            return null;
        }
        
        private PlayerAttackAction CreateAttackAction(PlayerCharacter playerCharacter, CreatureController creatureController)
        {
            PlayerAttackAction attackAction;
            switch (playerCharacter.weapon.WeaponType)
            {
                case WeaponType.Ranged:
                    attackAction = new PlayerRangedAttackAction();
                    break;
                default:
                case WeaponType.Melee:
                    if (_playerController.currentDistanceToTarget > Weapon.MeleeRange)
                    {
                        return null;
                    }
                    
                    attackAction = new PlayerMeleeAttackAction();
                    break;
            }

            attackAction.TargetCreature = creatureController;
            attackAction.PlayerCharacter = playerCharacter;
            
            return attackAction;
        }
    }
}