﻿using Runtime.Creatures;

namespace Runtime.Player.Actions
{
    public abstract class PlayerAttackAction : PlayerAction
    {
        public CreatureController TargetCreature;
    }
}