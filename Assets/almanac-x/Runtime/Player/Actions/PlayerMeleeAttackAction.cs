﻿using Cysharp.Threading.Tasks;

namespace Runtime.Player.Actions
{
    public class PlayerMeleeAttackAction : PlayerAttackAction
    {
        public override async UniTask Perform()
        {
            if (TargetCreature)
            {
                TargetCreature.TakeDamage(PlayerCharacter.weapon.attackDice.Roll());
            }

            // World.Get().hudController.weaponPanel.PlayWeaponAnimation(PlayerCharacter.weapon.weaponClass);
            //
            // await UniTask.Delay(TimeSpan.FromSeconds(0.1f));
        }
    }
}