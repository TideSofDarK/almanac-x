﻿using Cysharp.Threading.Tasks;
using Runtime.Model;

namespace Runtime.Player.Actions
{
    public abstract class PlayerAction
    {
        public PlayerCharacter PlayerCharacter;

        public abstract UniTask Perform();
    }
}