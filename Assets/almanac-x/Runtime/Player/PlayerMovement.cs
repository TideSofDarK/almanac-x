﻿using UnityEngine;

namespace Runtime.Player
{
    public class PlayerMovement
    {
        class CameraState
        {
            public float Yaw;
            public float Pitch;
            public float Roll;

            public void SetFromTransform(Transform t)
            {
                var eulerAngles = t.eulerAngles;
                Pitch = eulerAngles.x;
                Yaw = eulerAngles.y;
                Roll = eulerAngles.z;
            }

            public void LerpTowards(CameraState target, float rotationLerpPct)
            {
                Yaw = Mathf.Lerp(Yaw, target.Yaw, rotationLerpPct);
                Pitch = Mathf.Lerp(Pitch, target.Pitch, rotationLerpPct);
                Roll = Mathf.Lerp(Roll, target.Roll, rotationLerpPct);
            }

            public void UpdateTransform(Transform t)
            {
                t.eulerAngles = new Vector3(Pitch, Yaw, Roll);
            }
        }
    
        private CameraState _targetCameraState = new CameraState();
        private CameraState _interpolatingCameraState = new CameraState();

        public float boost = 4.75f;
        public float shiftBoost = 8f;
        public float jumpHeight = 6.2f;
        public float gravity = -0.3905f;

        public AnimationCurve mouseSensitivityCurve =
            new AnimationCurve(new Keyframe(0f, 0.5f, 0f, 5f), new Keyframe(1f, 2.5f, 0f, 0f));

        public float rotationLerpTime = 0.01f;

        public bool invertY = false;

        private readonly Transform _transform;
        private readonly CharacterController _characterController;

        private Vector3 _movement;

        public PlayerMovement(CharacterController characterController)
        {
            _transform = characterController.transform;
            _characterController = characterController;
            
            _targetCameraState.SetFromTransform(_transform);
            _interpolatingCameraState.SetFromTransform(_transform);
        }

        Vector3 GetInputTranslationDirection()
        {
            Vector3 direction = new Vector3();
            if (Input.GetKey(KeyCode.W))
            {
                direction += Vector3.forward;
            }

            if (Input.GetKey(KeyCode.S))
            {
                direction += Vector3.back;
            }

            if (Input.GetKey(KeyCode.A))
            {
                direction += Vector3.left;
            }

            if (Input.GetKey(KeyCode.D))
            {
                direction += Vector3.right;
            }

            return direction;
        }

        public void UpdateLook()
        {
            // Hide and lock cursor when right mouse button pressed
            if (Input.GetMouseButtonDown(1))
            {
                Cursor.lockState = CursorLockMode.Locked;
            }

            // Unlock and show cursor when right mouse button released
            if (Input.GetMouseButtonUp(1))
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            
            // Rotation
            if (Input.GetMouseButton(1))
            {
                var mouseMovement = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y") * (invertY ? 1 : -1));

                var mouseSensitivityFactor = mouseSensitivityCurve.Evaluate(mouseMovement.magnitude);

                _targetCameraState.Yaw += mouseMovement.x * mouseSensitivityFactor;
                _targetCameraState.Pitch += mouseMovement.y * mouseSensitivityFactor;
            }
            
            // Framerate-independent interpolation
            // Calculate the lerp amount, such that we get 99% of the way to our target in the specified time
            var rotationLerpPct = 1f - Mathf.Exp((Mathf.Log(1f - 0.99f) / rotationLerpTime) * Time.deltaTime);
            _interpolatingCameraState.LerpTowards(_targetCameraState, rotationLerpPct);
        
            _interpolatingCameraState.UpdateTransform(_transform);
        }

        public void UpdateMovement()
        {
            // Exit Sample  
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#endif
            }

            // Translation
            var translation = _transform.TransformDirection(GetInputTranslationDirection());

            // Speed up movement when shift key held
            if (Input.GetKey(KeyCode.LeftShift))
            {
                translation *= shiftBoost;
            }
            else
            {
                translation *= boost;
            }
            
            if (Input.GetKeyDown(KeyCode.Space) && _characterController.isGrounded)
            {
                _movement.y += jumpHeight;
            }

            _movement.x = translation.x;
            _movement.z = translation.z;
        }

        public void ApplyMovementAndGravity()
        {
            if (_characterController.isGrounded && _movement.y < 0)
            {
                _movement.y = 0f;
            }
            _movement.y += gravity;
            _characterController.Move(_movement * Time.deltaTime);
        }
    }
}