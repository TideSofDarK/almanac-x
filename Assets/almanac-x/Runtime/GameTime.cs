﻿using System;
using System.Globalization;
using UnityEngine.Events;

namespace Runtime
{
    public class GameTime
    {
        public DateTime Time;

        public readonly UnityEvent OnTimeChanged = new UnityEvent();

        public void Advance1Second()
        {
            Time += TimeSpan.FromSeconds(1);
            OnTimeChanged.Invoke();
        }

        public override string ToString()
        {
            return Time.ToString(CultureInfo.InvariantCulture);
        }
    }
}