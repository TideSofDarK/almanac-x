﻿using System;
using Cysharp.Threading.Tasks;
using Runtime.Utils;
using UnityEngine;

namespace Runtime.Effects
{
    public class EffectsManager : MonoBehaviour
    {
        public GameObject bloodSplashPrefab;

        private SimplePool<ParticleSystem> _bloodSplashPool;

        private void Awake()
        {
            _bloodSplashPool = new SimplePool<ParticleSystem>(bloodSplashPrefab, 10);
        }

        public async UniTaskVoid PlayBloodSplash(Vector3 position)
        {
            var bloodSplash = _bloodSplashPool.Get();
            bloodSplash.transform.position = position;
            var lifetime = bloodSplash.main.startLifetime.constant;
            
            bloodSplash.Play();

            await UniTask.Delay(TimeSpan.FromSeconds(lifetime));
            
            _bloodSplashPool.Return(bloodSplash);
        }
    }
}