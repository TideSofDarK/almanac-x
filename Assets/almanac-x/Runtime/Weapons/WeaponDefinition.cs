﻿using Runtime.Model;
using UnityEngine;

namespace Runtime.Weapons
{
    [CreateAssetMenu(fileName = "WeaponDefinition", menuName = "almanac/Weapon Definition", order = 0)]
    public class WeaponDefinition : ScriptableObject
    {
        public Weapon model;
        public AnimationClip animationClip;
        public Sprite icon;
    }
}