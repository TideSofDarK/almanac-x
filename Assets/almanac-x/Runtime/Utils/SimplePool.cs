﻿using System.Collections.Generic;
using UnityEngine;

namespace Runtime.Utils
{
    public class SimplePool<T> where T : Component
    {
        private Queue<T> _pool;
        private GameObject _prefab;

        public SimplePool(GameObject prefab, int initialQuantity)
        {
            _pool = new Queue<T>();
            
            for (var i = 0; i < initialQuantity; i++)
            {
                var go = Object.Instantiate(prefab);
                go.SetActive(false);
                _pool.Enqueue(go.GetComponent<T>());
            }
        }

        public T Get()
        {
            var element = _pool.Dequeue();
            element.gameObject.SetActive(true);
            return element;
        }

        public void Return(T objectToReturn)
        {
            objectToReturn.gameObject.SetActive(false);
            _pool.Enqueue(objectToReturn);
        }
    }
}