﻿using UnityEngine;

namespace Runtime.Utils
{
    public static class MathUtils
    {
        public static float Repeat(float t, float length, float min = 0.0f) =>
            Mathf.Clamp(t - Mathf.Floor(t / length) * length, min, length);
    }
}