﻿using UnityEngine;

namespace Runtime.Utils
{
    public class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        public static T Get()
        {
            return _instance;
        }

        protected virtual void Awake()
        {
            _instance = GetInstance();
        }

        protected T GetInstance() {
            return this as T;
        }
    }
}