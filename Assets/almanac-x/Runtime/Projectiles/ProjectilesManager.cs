﻿using Cysharp.Threading.Tasks;
using Runtime.Creatures;
using Runtime.Utils;
using UnityEngine;

namespace Runtime.Projectiles
{
    public class ProjectilesManager : MonoBehaviour
    {
        [SerializeField] private GameObject arrowPrefab;

        private SimplePool<Projectile> _arrowPool;

        public async UniTask<CreatureController> SpawnArrow(Vector3 start, Vector3 direction)
        {
            var projectile = _arrowPool.Get();
            var creatureController = await projectile.Initialize(start, direction);
            _arrowPool.Return(projectile);
            return creatureController;
        }

        private void Awake()
        {
            _arrowPool = new SimplePool<Projectile>(arrowPrefab, 4);
        }
    }
}