﻿using System;
using Cysharp.Threading.Tasks;
using Runtime.Creatures;
using UnityEngine;

namespace Runtime.Projectiles
{
    public class Projectile : MonoBehaviour
    {
        public float lifetime;
        public float speed;
        private UniTaskCompletionSource<CreatureController> _uniTaskCompletionSource;
        
        public async UniTask<CreatureController> Initialize(Vector3 start, Vector3 direction)
        {
            transform.position = start;
            transform.rotation = Quaternion.LookRotation(direction);
            
            _uniTaskCompletionSource = new UniTaskCompletionSource<CreatureController>();

            var result = await _uniTaskCompletionSource.Task.TimeoutWithoutException(TimeSpan.FromSeconds(lifetime));

            return result.Result;
        }

        private void Update()
        {
            transform.Translate( Vector3.forward * (speed * Time.deltaTime));
        }

        private void OnTriggerEnter(Collider other)
        {
            other.TryGetComponent<CreatureController>(out var creatureController);
            if (creatureController)
            {
                _uniTaskCompletionSource.TrySetResult(creatureController);
            }
        }
    }
}