﻿using Runtime.Creatures.AI;
using UnityEngine;

namespace Runtime.Creatures
{
    public class CreatureManager
    {
        public readonly Blackboard Blackboard;

        private readonly CreatureController[] _creatures;

        public CreatureManager()
        {
            Blackboard = new Blackboard();

            _creatures = Object.FindObjectsOfType<CreatureController>();
            foreach (var t in _creatures)
            {
                t.InitializeCreature();
            }
        }

        public void UpdateRealtime()
        {
            foreach (var t in _creatures)
            {
                if (!t.Model.IsDead())
                {
                    t.UpdateAi();
                }
            }
        }

        public void SetTurnBasedBlock(bool isBlocked)
        {
            foreach (var t in _creatures)
            {
                t.SetPathfindingFrozen(isBlocked);
            }
        }

        public void RenderAll()
        {
            var cameraForward = World.Get().playerController.transform.forward;
            var cameraAngle = Mathf.Atan2(cameraForward.x, cameraForward.z) * Mathf.Rad2Deg;

            foreach (var t in _creatures)
            {
                t.Render(cameraAngle);
            }
        }
    }
}