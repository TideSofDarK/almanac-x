﻿using Runtime.Model;
using UnityEngine;

namespace Runtime.Creatures.Definitions
{
    [CreateAssetMenu(fileName = "CreatureAbilitiesDefinition", menuName = "almanac/Creature Abilities Definition", order = 0)]
    public class CreatureAbilitiesDefinition : ScriptableObject
    {
        public int maxHealth;
        
        [Header("Melee Attack")]
        public bool hasMeleeAttack;
        public float meleeAttackRange;
        public Dice attackDice;
    }
}