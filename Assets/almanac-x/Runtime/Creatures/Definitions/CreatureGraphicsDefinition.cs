﻿using System;
using UnityEngine;

namespace Runtime.Creatures.Definitions
{
    [Serializable]
    public struct FiveDirAnimation
    {
        public Texture2D sheet0;
        public Texture2D sheet45;
        public Texture2D sheet90;
        public Texture2D sheet135;
        public Texture2D sheet180;
        public int frames;
        public bool sheet0Only;
        public bool loop;
    }
    
    [CreateAssetMenu(fileName = "CreatureGraphicsDefinition", menuName = "almanac/Creature Graphics Definition", order = 0)]
    public class CreatureGraphicsDefinition : ScriptableObject
    {
        public FiveDirAnimation move;
        public FiveDirAnimation attack;
        public FiveDirAnimation death;
        public FiveDirAnimation flinch;

        public FiveDirAnimation GetFiveDirAnimation(CreatureAnimationType creatureAnimationType)
        {
            switch (creatureAnimationType)
            {
                case CreatureAnimationType.Death:
                    return death;
                case CreatureAnimationType.Attack:
                    return attack;
                case CreatureAnimationType.Flinch:
                    return flinch;
                case CreatureAnimationType.Move:
                default:
                    return move;
            }
        }
    }
}