﻿using UnityEngine;

namespace Runtime.Creatures.Definitions
{
    [CreateAssetMenu(fileName = "CreatureAiDefinition", menuName = "almanac/Creature AI Definition", order = 0)]
    public class CreatureAiDefinition : ScriptableObject
    {
        public float chaseRadius = 10;
        public float chaseThreshold = 20;

        public float roamRadius = 10;
        public float roamIdleTimeMin = 2f;
        public float roamIdleTimeMax = 4f;
    }
}