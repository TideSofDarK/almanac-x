﻿namespace Runtime.Creatures
{
    public enum CreatureDirection
    {
        N,
        NE,
        E,
        SE,
        S,
        SW,
        W,
        NW
    }
}