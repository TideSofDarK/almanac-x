﻿namespace Runtime.Creatures
{
    public enum CreatureAiState
    {
        Idle,
        Roam,
        Chase,
        Attack
    }
}