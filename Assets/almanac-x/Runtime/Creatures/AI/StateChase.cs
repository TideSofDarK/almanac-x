﻿using UnityEngine;

namespace Runtime.Creatures.AI
{
    public class StateChase : StateBase
    {
        private IAiTarget _aiTarget;

        public StateChase(IAiTarget aiTarget)
        {
            _aiTarget = aiTarget;
        }

        public override void OnEnter()
        {
            CreatureController.SetDestination(_aiTarget.GetAiTargetPosition());
        }

        public override void Update()
        {
            if (CreatureController.creatureAbilitiesDefinition.hasMeleeAttack)
            {
                if (Vector3.Distance(CreatureController.transform.position, _aiTarget.GetAiTargetPosition()) <=
                    CreatureController.creatureAbilitiesDefinition.meleeAttackRange)
                {
                    CreatureController.SetAttacking(_aiTarget);
                    return;
                }
            }

            CreatureController.SetDestination(_aiTarget.GetAiTargetPosition());
        }
    }
}