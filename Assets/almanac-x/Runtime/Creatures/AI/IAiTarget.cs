﻿using Runtime.Model;
using UnityEngine;

namespace Runtime.Creatures.AI
{
    public interface IAiTarget
    {
        Vector3 GetAiTargetPosition();
        CreatureBase GetAttackTarget();
    }
}