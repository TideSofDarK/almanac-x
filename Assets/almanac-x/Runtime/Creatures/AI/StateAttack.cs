﻿using UnityEngine;

namespace Runtime.Creatures.AI
{
    public class StateAttack : StateBase
    {
        private IAiTarget _aiTarget;

        public StateAttack(IAiTarget aiTarget)
        {
            _aiTarget = aiTarget;
        }
        
        public override void OnEnter()
        {
            SetupAttack();
            CreatureController.CancelDestination();
        }

        public override void Update()
        {
            if (!CreatureController.IsAnimationPlaying())
            {
                OnAnimationFinished();
            }
        }

        private void SetupAttack()
        {
            CreatureController.SetAnimation(CreatureAnimationType.Attack);
            CreatureController.LookAtPosition(_aiTarget.GetAiTargetPosition());
        }

        private void OnAnimationFinished()
        {
            _aiTarget.GetAttackTarget().TakeDamage(CreatureController.Model.AttackDice.Roll());
            if (Vector3.Distance(CreatureController.transform.position, _aiTarget.GetAiTargetPosition()) <=
                CreatureController.creatureAbilitiesDefinition.meleeAttackRange)
            {
                SetupAttack();
            }
            else
            {
                CreatureController.SetRoaming();
            }
        }
    }
}