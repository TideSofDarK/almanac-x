﻿namespace Runtime.Creatures.AI
{
    public class StateFlinch : StateBase
    {
        public override void OnEnter()
        {
            CreatureController.SetAnimation(CreatureAnimationType.Flinch);
            CreatureController.CancelDestination();
        }

        public override void Update()
        {
            if (!CreatureController.IsAnimationPlaying())
            {
                OnAnimationFinished();
            }
        }

        private void OnAnimationFinished()
        {
            CreatureController.SetRoaming();
        }
    }
}