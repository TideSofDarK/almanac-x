﻿using UnityEngine;

namespace Runtime.Creatures.AI
{
    public class StateRoam : StateBase
    {
        private Vector3 _startPosition;
        private float _nextRoamTime;

        public override void OnEnter()
        {
            _startPosition = CreatureController.transform.position;
            
            FindNextPosition();
        }

        public override void Update()
        {
            if (CreatureController.IsAiIdle())
            {
                if (Time.time > _nextRoamTime)
                {
                    FindNextPosition();
                }
                else
                {
                    CreatureController.SetAnimationPlaying(false);
                }
            }
            else
            {
                if (Vector3.Distance(World.Get().CreatureManager.Blackboard.DefaultAiTarget.GetAiTargetPosition(), CreatureController.transform.position) < CreatureController.creatureAiDefinition.chaseRadius)
                {
                    CreatureController.SetChasing(World.Get().CreatureManager.Blackboard.DefaultAiTarget);
                }
            }
        }

        private void FindNextPosition()
        {
            Vector3 offset = Random.insideUnitSphere * Random.Range(10f, 25f);
            offset.y = 0;
            CreatureController.SetDestination(_startPosition + offset, true);
            _nextRoamTime = Time.time + Random.Range(CreatureController.creatureAiDefinition.roamIdleTimeMin,
                CreatureController.creatureAiDefinition.roamIdleTimeMax);
            CreatureController.SetAnimation(CreatureAnimationType.Move, true);
        }
    }
}