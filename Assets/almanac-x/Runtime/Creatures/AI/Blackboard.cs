﻿namespace Runtime.Creatures.AI
{
    public class Blackboard
    {
        public IAiTarget DefaultAiTarget;
        
        public Blackboard()
        {
            DefaultAiTarget = World.Get().playerController;
        }
    }
}