﻿namespace Runtime.Creatures.AI
{
    public abstract class StateBase
    {
        protected CreatureController CreatureController;

        public void InitializeState(CreatureController creatureController)
        {
            CreatureController = creatureController;
        }

        public abstract void OnEnter();

        public abstract void Update();
    }
}