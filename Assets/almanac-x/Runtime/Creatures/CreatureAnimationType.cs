﻿namespace Runtime.Creatures
{
    public enum CreatureAnimationType
    {
        Move,
        Attack,
        Flinch,
        Death,
        Custom1,
        Custom2
    }
}