﻿using System;
using Runtime.Creatures.Definitions;
using Runtime.Utils;
using UnityEngine;

namespace Runtime.Creatures
{
    [Serializable]
    public class CreatureRenderer
    {
        private static readonly int SheetOptionsID = Shader.PropertyToID("_SheetOptions");
        private static readonly int MainTexID = Shader.PropertyToID("_MainTex");
        
        public bool IsAnimationPlaying { get; private set; }
        
        private Transform _transform;
        private CreatureGraphicsDefinition _creatureGraphicsDefinition;
        private CreatureAnimationType _currentAnimationType;
        private float _frameDuration = 0.1155f;
        private float _timeUntilFrameChange;
        private Material _material;
        private Vector4 _frameCountAndCurrentIndex = Vector4.zero;
        private FiveDirAnimation _currentFiveDirAnimation;
        private int _lastSheetIndex = -1;

        public CreatureRenderer(Transform transform, Material material, CreatureGraphicsDefinition creatureGraphicsDefinition)
        {
            _transform = transform;
            _material = material;
            _creatureGraphicsDefinition = creatureGraphicsDefinition;
            
            SetAnimation(CreatureAnimationType.Move);
        }

        public void SetAnimation(CreatureAnimationType creatureAnimationType, bool play = true)
        {
            _currentAnimationType = creatureAnimationType;
            _currentFiveDirAnimation = _creatureGraphicsDefinition.GetFiveDirAnimation(creatureAnimationType);
            
            if (_currentFiveDirAnimation.sheet0Only)
            {
                _material.SetTexture(MainTexID, _currentFiveDirAnimation.sheet0);
            }
            else
            {
                _lastSheetIndex = -1;
            }

            _frameCountAndCurrentIndex.x = _currentFiveDirAnimation.frames;
            _frameCountAndCurrentIndex.y = 0;
            SetAnimationPlaying(play);
        }

        public void SetAnimationPlaying(bool play)
        {
            if (!IsAnimationPlaying && play)
            {
                _timeUntilFrameChange = Time.time + _frameDuration;
            }

            if (!play)
            {
                _frameCountAndCurrentIndex.y = 0;
            }

            IsAnimationPlaying = play;
        }

        public void Render(float angle)
        {
            if (!_creatureGraphicsDefinition)
            {
                Debug.LogWarning("Trying to render but no Graphics Definition supplied");
                return;
            }

            if (IsAnimationPlaying)
            {
                if (Time.time > _timeUntilFrameChange)
                {
                    _timeUntilFrameChange = Time.time + _frameDuration;
                    _frameCountAndCurrentIndex.y =
                        MathUtils.Repeat(_frameCountAndCurrentIndex.y + 1, _frameCountAndCurrentIndex.x, 1.0f);
                    if (!_currentFiveDirAnimation.loop && Math.Abs(_frameCountAndCurrentIndex.y - (_frameCountAndCurrentIndex.x - 1)) < 0.1f)
                    {
                        IsAnimationPlaying = false;
                    }
                }
            }

            UpdateTexture(angle);
            UpdateSheetData();
        }

        private void UpdateSheetData()
        {
            _material.SetVector(SheetOptionsID, _frameCountAndCurrentIndex);
        }

        private void UpdateTexture(float cameraAngle)
        {
            if (_currentFiveDirAnimation.sheet0Only)
            {
                return;
            }
            
            var forward = _transform.rotation * -Vector3.forward;
            var angle = Mathf.DeltaAngle(cameraAngle, Mathf.Atan2(forward.x, forward.z) * Mathf.Rad2Deg);

            var flipped = false;
            var sheetIndex = Mathf.RoundToInt(Mathf.Abs(angle / 180) * 4);
            if (sheetIndex != 0 && sheetIndex != 4)
            {
                flipped = angle > 0.0f;
            }

            _frameCountAndCurrentIndex.z = flipped ? 0.0f : 1.0f;

            if (_lastSheetIndex != sheetIndex)
            {
                _lastSheetIndex = sheetIndex;
                switch (_lastSheetIndex)
                {
                    case 0:
                        _material.SetTexture(MainTexID, _currentFiveDirAnimation.sheet0);
                        break;
                    case 1:
                        _material.SetTexture(MainTexID, _currentFiveDirAnimation.sheet45);
                        break;
                    case 2:
                        _material.SetTexture(MainTexID, _currentFiveDirAnimation.sheet90);
                        break;
                    case 3:
                        _material.SetTexture(MainTexID, _currentFiveDirAnimation.sheet135);
                        break;
                    case 4:
                        _material.SetTexture(MainTexID, _currentFiveDirAnimation.sheet180);
                        break;
                    default:
                        _material.SetTexture(MainTexID, _currentFiveDirAnimation.sheet0);
                        break;
                }
            }
        }
    }
}