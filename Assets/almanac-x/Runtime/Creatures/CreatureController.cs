﻿using System;
using Pathfinding;
using Pathfinding.RVO;
using Runtime.Creatures.AI;
using Runtime.Creatures.Definitions;
using Runtime.Model;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Runtime.Creatures
{
    public class CreatureController : MonoBehaviour
    {
        public CreatureGraphicsDefinition creatureGraphicsDefinition;
        public CreatureAiDefinition creatureAiDefinition;
        public CreatureAbilitiesDefinition creatureAbilitiesDefinition;

        [Space] public new Collider collider;
        public new Rigidbody rigidbody;
        public MeshRenderer meshRenderer;
        public AIPath aiPath;
        public Seeker seeker;
        public RVOController rvoController;

        [NonSerialized] public Monster Model;

        private StateBase _ai;
        [SerializeField] private int debugStateId = -1;
        private CreatureRenderer _creatureRenderer;

        public void InitializeCreature()
        {
            Model = new Monster(creatureAbilitiesDefinition.maxHealth, creatureAbilitiesDefinition.attackDice);

            _creatureRenderer = new CreatureRenderer(transform, meshRenderer.material, creatureGraphicsDefinition);
            
            SetRoaming();
        }

        public void LookAtPosition(Vector3 position)
        {
            var targetPosition = new Vector3(position.x,
                transform.position.y,
                position.z);
            transform.LookAt(targetPosition);
        }

        public Vector3 GetMiddlePoint()
        {
            return meshRenderer.bounds.center;
        }

        public void SetDestination(Vector3 destination, bool searchImmediately = false)
        {
            aiPath.destination = destination;
            if (searchImmediately)
            {
                aiPath.SearchPath();
            }
        }
        
        public void CancelDestination()
        {
            aiPath.destination = Vector3.positiveInfinity;
            aiPath.SetPath(null);
        }

        public void SetPathfindingFrozen(bool isFrozen)
        {
            aiPath.enabled = !isFrozen;
            seeker.enabled = !isFrozen;
        }

        public bool IsAiIdle()
        {
            return !aiPath.pathPending && (aiPath.reachedDestination || !aiPath.hasPath);
        }
        
        private void SetState(StateBase stateBase)
        {
            stateBase.InitializeState(this);
            _ai = stateBase;
            _ai.OnEnter();
        }

        public void SetIdle()
        {
            debugStateId = 0;
            SetState(new StateIdle());
        }

        public void SetRoaming()
        {
            debugStateId = 1;
            SetState(new StateRoam());
        }

        public void SetChasing(IAiTarget aiTarget)
        {
            debugStateId = 2;
            SetState(new StateChase(aiTarget));
        }

        public void SetAttacking(IAiTarget aiTarget)
        {
            debugStateId = 3;
            SetState(new StateAttack(aiTarget));
        }
        
        public void SetFlinching()
        {
            debugStateId = 4;
            SetState(new StateFlinch());
        }

        public void UpdateAi()
        {
            _ai.Update();
        }

        public bool IsAnimationPlaying()
        {
            return _creatureRenderer.IsAnimationPlaying;
        }

        public void SetAnimationPlaying(bool play)
        {
            _creatureRenderer.SetAnimationPlaying(play);
        }

        public void SetAnimation(CreatureAnimationType creatureAnimationType, bool play = true)
        {
            _creatureRenderer.SetAnimation(creatureAnimationType, play);
        }

        public void Render(float angle)
        {
            _creatureRenderer.Render(angle);
        }
        
        public void TakeDamage(int damage)
        {
            TakeDamage(damage, GetMiddlePoint());
        }

        public void TakeDamage(int damage, Vector3 point)
        {
            if (Model.IsDead())
            {
                Debug.LogWarning($"Attempting to damage dead creature: {transform.name}");
                return;
            }
            
            Model.TakeDamage(damage);
            
            World.Get().effectsManager.PlayBloodSplash(point + Random.insideUnitSphere*0.1f).Forget();

            if (Model.IsDead())
            {
                Die();
            }
            else
            {
                if (!(_ai is StateAttack))
                {
                    SetFlinching();
                }
            }
        }

        public void HitImpact(Vector3 direction)
        {
            rigidbody.isKinematic = false;

            direction.y = 2f;
            rigidbody.AddForce(direction * 2f, ForceMode.Impulse);
        }

        public void Die()
        {
            collider.gameObject.layer = WorldUtils.RagdollLayer;
            
            _creatureRenderer.SetAnimation(CreatureAnimationType.Death);
            
            SetPathfindingFrozen(true);
            rvoController.enabled = false;

            SetIdle();
        }
    }
}