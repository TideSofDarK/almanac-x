﻿using Runtime.Model;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace Runtime.HUD
{
    public class WeaponPanel : MonoBehaviour
    {
        public GameObject crosshair;
        public Animator animator;
        
        public AnimationClip swordClip, crossbowClip;

        private PlayableGraph _playableGraph;
        private PlayableOutput _playableOutput;

        private void OnEnable()
        {
            _playableGraph = PlayableGraph.Create();
            _playableGraph.SetTimeUpdateMode(DirectorUpdateMode.GameTime);
            _playableOutput = AnimationPlayableOutput.Create(_playableGraph, "Animation", animator);
        }
        
        private void OnDisable()
        {
            _playableGraph.Destroy();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(1))
            {
                crosshair.SetActive(true);
            }
            else if (Input.GetMouseButtonUp(1))
            {
                crosshair.SetActive(false);
            }
        }

        public void PlayWeaponAnimation(WeaponClass weaponClass)
        {
            var clipPlayable = AnimationClipPlayable.Create(_playableGraph, GetAnimationClipByWeaponClass(weaponClass));
            _playableOutput.SetSourcePlayable(clipPlayable);
            _playableGraph.Play();
        }

        private AnimationClip GetAnimationClipByWeaponClass(WeaponClass weaponClass)
        {
            switch (weaponClass)
            {
                case WeaponClass.Crossbow:
                    return crossbowClip;
                default:
                case WeaponClass.Sword:
                    return swordClip;
            }
        }

        private void OnWeaponImpact()
        {
            
        }
    }
}