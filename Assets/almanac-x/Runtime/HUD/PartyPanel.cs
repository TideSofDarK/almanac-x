﻿using DG.Tweening;
using Runtime.Model;
using Runtime.Weapons;
using UnityEngine;

namespace Runtime.HUD
{
    public class PartyPanel : MonoBehaviour
    {
        public CharacterPortrait[] portraits;
        
        private Party _party;

        private void Start()
        {
            _party = World.Get().playerController.party;

            UpdateCooldowns();
            UpdateCurrentCharacter(World.Get().playerController.currentCharacterIndex);
            UpdateHealthbars();

            World.Get().GameTime.OnTimeChanged.AddListener(UpdateCooldowns);
            World.Get().playerController.OnCharacterSetOnCooldown.AddListener(UpdateCooldown);
            World.Get().playerController.OnCurrentCharacterChanged.AddListener(UpdateCurrentCharacter);
            World.Get().playerController.OnWeaponEquipped.AddListener(UpdateEquippedWeapon);
            _party.OnPartyDamageTaken.AddListener(UpdateHealthbars);
        }

        private void OnDestroy()
        {
            World.Get().GameTime.OnTimeChanged.RemoveListener(UpdateCooldowns);
            World.Get().playerController.OnCharacterSetOnCooldown.RemoveListener(UpdateCooldown);
            World.Get().playerController.OnCurrentCharacterChanged.RemoveListener(UpdateCurrentCharacter);
            World.Get().playerController.OnWeaponEquipped.RemoveListener(UpdateEquippedWeapon);
            _party.OnPartyDamageTaken.RemoveListener(UpdateHealthbars);
        }

        private void UpdateCooldown(int characterIndex)
        {
            var now = World.Get().GameTime.Time;
            var cooldown = (float) (_party.Characters[characterIndex].OnCooldownUntil - now).TotalSeconds;
            var fill = 1f - (cooldown / _party.Characters[characterIndex].CooldownDuration);
            portraits[characterIndex].cooldownFillImage.fillAmount = fill;
        }

        private void UpdateCooldowns()
        {
            for (int i = 0; i < Party.MaxCharacters; i++)
            {
                UpdateCooldown(i);
            }
        }

        private void UpdateCurrentCharacter(int characterIndex)
        {
            for (var i = 0; i < portraits.Length; i++)
            {
                portraits[i].SetTurnActive(i == characterIndex);
            }
        }

        private void UpdateHealthbars()
        {
            for (int i = 0; i < Party.MaxCharacters; i++)
            {
                var fill = (float) _party.Characters[i].Health / _party.Characters[i].MaxHealth;
                portraits[i].healthFillImage.DOFillAmount(Mathf.Max(fill, 0f), 0.25f);
            }
        }

        private void UpdateEquippedWeapon(int characterIndex, WeaponDefinition weaponDefinition)
        {
            portraits[characterIndex].SetEquippedWeaponIcon(weaponDefinition.icon);
        }
    }
}