﻿using TMPro;
using UnityEngine;

namespace Runtime.HUD
{
    public class HudController : MonoBehaviour
    {
        public HourglassPanel hourglassPanel;
        public WeaponPanel weaponPanel;
        public TMP_Text pointerCaption;
        public TMP_Text debugTimeText;

        private void UpdateDebugTime()
        {
            debugTimeText.text = World.Get().GameTime.ToString();
        }

        private void OnEnable()
        {
            World.Get().GameTime.OnTimeChanged.AddListener(UpdateDebugTime);
        }

        private void OnDisable()
        {
            World.Get().GameTime.OnTimeChanged.RemoveListener(UpdateDebugTime);
        }
    }
}