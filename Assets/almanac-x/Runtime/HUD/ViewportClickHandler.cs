﻿using UnityEngine;

namespace Runtime.HUD
{
    public class ViewportClickHandler : MonoBehaviour
    {
        [SerializeField] public RectTransform rectTransform;
        [SerializeField] public new Camera camera;

        public Vector2 GetViewportLocalPointerPosition()
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, Input.mousePosition,
                null, out var local);
            return new Vector2(0.5f, 0.5f) + local / (rectTransform.rect.size);
        }

        public Ray GetViewportRay()
        {
            return camera.ViewportPointToRay(GetViewportLocalPointerPosition());
        }
    }
}