﻿using UnityEngine;
using UnityEngine.UI;

namespace Runtime.HUD
{
    public class CharacterPortrait : MonoBehaviour
    {
        public Image healthFillImage;
        public Image manaFillImage;
        public Image cooldownFillImage;
        
        [SerializeField] private Image turnIcon;
        [SerializeField] private Image weaponIcon;

        public void SetEquippedWeaponIcon(Sprite weaponSprite)
        {
            weaponIcon.sprite = weaponSprite;
            weaponIcon.SetNativeSize();
        }

        public void SetTurnActive(bool isActive)
        {
            turnIcon.gameObject.SetActive(isActive);
        }
    }
}