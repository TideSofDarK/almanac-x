﻿using UnityEngine;
using UnityEngine.UI;

namespace Runtime.HUD
{
    public class HourglassPanel : MonoBehaviour
    {
        public float animationRate = 0.5f;
        
        [SerializeField] public Image hourglassImage;
        [SerializeField] private Sprite[] sprites;

        private int _currentIndex;
        private float _timeUntilFrameChange;
        private bool _isPlaying = true;

        private void OnEnable()
        {
            World.Get().OnCreatureTurn.AddListener(SetPlaying);
            World.Get().OnTimeModeChanged.AddListener(SetPlaying);
            World.Get().OnPlayerTurn.AddListener(SetNotPlaying);
        }

        private void OnDisable()
        {
            World.Get().OnCreatureTurn.RemoveListener(SetPlaying);
            World.Get().OnTimeModeChanged.RemoveListener(SetPlaying);
            World.Get().OnPlayerTurn.RemoveListener(SetNotPlaying);
        }

        private void SetPlaying()
        {
            _isPlaying = true;
        }
        
        private void SetPlaying(TimeMode timeMode)
        {
            _isPlaying = timeMode == TimeMode.Realtime;
        }

        private void SetNotPlaying()
        {
            _isPlaying = false;
        }
        
        private void Update()
        {
            if (!_isPlaying || !(Time.time > _timeUntilFrameChange)) return;
            
            _timeUntilFrameChange = Time.time + animationRate;

            _currentIndex = (int) Mathf.Repeat(_currentIndex + 1, sprites.Length - 1);
                
            hourglassImage.sprite = sprites[_currentIndex];
        }
    }
}