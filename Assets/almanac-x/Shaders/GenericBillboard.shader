﻿Shader "almanac/Generic Billboard"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_SheetOptions ("Sheet Options", Vector) = (9,0,0,0)
		_BillboardScale ("Scale", Vector) = (1,1,0,0)
	}
	SubShader
	{	

		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "DisableBatching" = "True" }

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 pos : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _SheetOptions;
			float2 _BillboardScale;

			void Unity_Flipbook_float(float2 UV, float Width, float Height, float Tile, float Invert, out float2 Out)
			{
			    Tile = fmod(Tile, Width * Height);
			    float2 tileCount = float2(1.0, 1.0) / float2(Width, Height);
			    float tileY = abs(-floor(Tile * tileCount.x));
			    float tileX = abs(Tile - Width * floor(Tile * tileCount.x));
				UV.x = UV.x - 2 * Invert * UV.x + Invert;
			    Out = (UV + float2(tileX, tileY)) * tileCount;
			}
		
			v2f vert (appdata v)
			{
				v2f o;
				o.uv = v.uv.xy;

				o.pos = mul(UNITY_MATRIX_P,
					mul(UNITY_MATRIX_MV, float4(0.0, 0.0, 0.0, 1.0))
					+ float4(v.vertex.x, v.vertex.y, 0.0, 0.0)
					* float4(_BillboardScale.x, _BillboardScale.y, 1.0, 1.0));

				UNITY_TRANSFER_FOG(o,o.pos);
				return o;
			}
		
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				float2 uv;
				Unity_Flipbook_float(i.uv, _SheetOptions[0], 1, _SheetOptions[1], _SheetOptions[2], uv);
				fixed4 col = tex2D(_MainTex, uv);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	} 
}